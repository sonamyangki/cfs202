const mongoose = require('mongoose')
const validator = require('validator')

const userSchema = new mongoose.Schema({
    name :{
        type: String,
        required : [true, 'Please provide your email!'],

    },
    email:{
        type:String,
        required: [true, 'Please provide your email'],
        unique: true,
        lowercase: true,
        validate:[validator.isEmail, 'Please provide a valid email'],

    },
    photo: {
        type: String,
        default: 'default.jpg',

    },
    role:{
        type: String,
        enum : ['user', 'sme', 'pharmacist', 'admin'],
        default: 'user'
    },
    password: {
        type: String,
        required: [true, 'Please provide password'],
        minlength: 8,
        //password wont be included when we get users
        select: false,
    },
    active: {
        type: Boolean,
        default: true,
        select: false,
    },

})

const User = mongoose.model('user', userSchema)
module.exports = User